'use strict';

module.exports = function(grunt) {

    var _defaultOptions = {
        processName : function(filepath){ return filepath; }
    };

    grunt.registerMultiTask("browserify-jst", "JST templates compiler for browserify", function(){
        var path = require('path');
        var _ = require('underscore');

        var config = grunt.config.get("browserify-jst");
        var cfg = config[this.target];
        var opts = cfg.options || {};
        _.defaults(opts, _defaultOptions);


        var templates = {};
        _.each(grunt.file.expand(cfg.src), function(templateFile){
            var t = grunt.file.read(templateFile);
            templates[opts.processName(templateFile)] = t;
        });
        var code = [
            'var _ = require("underscore");',
            'var templates = ' + JSON.stringify(templates) + ';',
            'module.exports = function(templateName){ return _.template(templates[templateName]); };'
        ].join('\n');

        grunt.file.write(cfg.dest, code);
    });
};